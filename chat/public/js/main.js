jQuery(function($){
	var socket = io.connect();
	var $nickForm = $('#setNick');
	var $nickError = $('#nickError');
	var $nickBox = $('#nickname');
	var $users = $('#users');
	var $messageForm = $('#send-message');
	var $messageBox = $('#message');
	var $chat = $('#chat');
	var $fontSize = $('#fontSize');
	var nick = '';
	var fontSize = 17;

	$fontSize.on('change', function () {
		fontSize = parseInt($fontSize.val());
		console.log(fontSize);
	});

	$nickForm.submit(function(e) {
		e.preventDefault();
		socket.emit('new user', $nickBox.val(), function(data){
			if (data){
				$('#nickWrap').hide();
				$('#contentWrap').removeClass('hidden');

			} else{
				$nickError.removeClass('hidden');
				$nickError.html('That username is already taken');
			}
		});
		nick = $nickBox.val();
		$nickBox.val('');
	});

	socket.on('usernames', function(data){
		var html = '';
		for(i=0; i < data.length; i++){
			html += data[i] + '<br/>'
		}
		$users.html(html);
	});

	$messageForm.submit(function(e){
		e.preventDefault();
		socket.emit('send message', $messageBox.val());
		$messageBox.val('');
	});
	socket.on('new message', function(data){
		$chat.append('<div class="message" style="font-size:' + fontSize + 'px;"><b>' + data.nick + ': </b>' + data.msg + "</div>");
		if (nick != data.nick) {
			notifyMe(data.nick, data.msg);
		}
	});

	function notifyMe(nick, text) {
		var options = {
	      	body: text
	  	}
	  // Let's check if the browser supports notifications
	  if (!("Notification" in window)) {
	    alert("This browser does not support desktop notification");
	  }

	  // Let's check whether notification permissions have already been granted
	  else if (Notification.permission === "granted") {
	    // If it's okay let's create a notification
	    var notification = new Notification(nick, options);
	  }

	  // Otherwise, we need to ask the user for permission
	  else if (Notification.permission !== 'denied') {
	    Notification.requestPermission(function (permission) {
	      // If the user accepts, let's create a notification
	      if (permission === "granted") {
	        var notification = new Notification(nick, options);
	      }
	    });
	  }
	}

	$( ".cover" ).fadeOut( 2500 );
});